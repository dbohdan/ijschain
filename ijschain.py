# ijschain.py -- A HexChat script that translates messages from #tcl
# bridge bots on Libera Chat. For a Tcl version, see
# <https://www.patthoyts.tk/xchat.html>. This script was written because
# HexChat no longer supports Tcl add-ons by default.
#
# Copyright (c) 2021, 2024 D. Bohdan <https://dbohdan.com/>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import re

import hexchat  # type: ignore

__module_name__ = "ijschain"
__module_author__ = "D. Bohdan, Sergey G. Brester"
__module_version__ = "0.4.0"
__module_description__ = "Handle bridge bots on #tcl"


DEBUG = False

BRIDGE_NICK = re.compile(r"^i[js]chain[_\d]*$")
BRIDGED_MESSAGE = re.compile(r"^[<\xab]?([^>\xbb]+?)[>\xbb]? (.*)$")
BRIDGED_ACTION = re.compile(r"^\*{1,3} [<\xab]?([^>\xbb]+?)[>\xbb]? (.*)$")
ACTION_JOIN_LEAVE = re.compile(
    r"(?:(?P<join>(?:has become available|joins|\*\*\* (?P<user_join>[^ ]+) joins))|"
    r"(?:has left|leaves|\*\*\* (?P<user_left>[^ ]+) leaves)"
    r")$",
)
VOICE_FAKE_NICKS = True


def command(s):
    if DEBUG:
        debug(f"  ## COMMAND: {s}")

    hexchat.command(s)


def debug(*args):
    if DEBUG:
        hexchat.prnt(*args)


def client_name(user, bot):
    """Format a fully-qualified fake user client name (with proper IRC escaping
    for the user name)."""
    name = re.sub(r"[^\w\-]+", "_", user)
    user = re.sub(r"[^\w_]+", "-", user)
    return f":{user}!~{name}@bridged/{bot}"


def message_callback(word, word_eol, event_name, _attributes):
    """Process channel messages and actions."""
    bot = hexchat.strip(word[0], -1, 1)  # Remove colors.

    if not BRIDGE_NICK.match(bot):
        return hexchat.EAT_NONE

    debug(f"## CALLBACK: {bot!r} {word!r} {event_name!r}")

    try:
        match = BRIDGED_MESSAGE.match(word[1])
        if not match:
            return hexchat.EAT_NONE
        user = match[1]
        message = match[2]

        # Convert the message to an action (`/me <action> ...`).
        if bot == user:
            match = BRIDGED_ACTION.match(message)
        elif re.match(r"^\*{1,3}$", user):
            match = BRIDGED_MESSAGE.match(message)
        else:
            match = None

        if match:
            user = match[1]
            message = match[2]
            event_name = re.sub(r"\b(?:Message|Msg)\b", "Action", event_name)

        if event_name.startswith("Channel Action"):
            # Handle a join or leave action.
            if handle_available_message(bot, user, message):
                return hexchat.EAT_ALL
        else:
            # Consider the user still unknown. Simulate a join action on the
            # first message from the user.
            handle_available_message(bot, user, {"join": "auto"})

        # Suppress highlighting for own messages processed by the bridge.
        if event_name.endswith(" Hilight") and user == hexchat.get_info("nick"):
            event_name = (
                event_name[: -len(" Hilight")]
                .replace("Channel ", "Your ")
                .replace(" Msg", " Message")
            )

        debug(f"  ## EMIT: {event_name}, {user}, {message}")
        hexchat.emit_print(event_name, user, message)
    except BaseException as e:  # noqa: BLE001
        hexchat.prnt(f"can't parse {bot} {event_name} `{word_eol[1]}`: {e}")

        if DEBUG:
            import traceback

            hexchat.prnt(traceback.format_exc())
    else:
        return hexchat.EAT_ALL

    return hexchat.EAT_NONE


def handle_available_message(bot, user, msg):
    """Process a join or leave action."""
    if not isinstance(msg, dict):
        # Parse the action message. (Detect whether it is a join/leave.).
        msg = ACTION_JOIN_LEAVE.match(msg)
        if not msg:
            return False
        msg = msg.groupdict()

        # Try to obtain the user name from the message.
        for grp, match in msg.items():
            if match and grp.startswith("user_"):
                user = match
                break

    # Try to find a user with this nick.
    users_list = hexchat.get_list("users")
    for list_user in users_list:
        if list_user.nick == user:
            break
    else:
        list_user = None

    client = client_name(user, bot)
    channel = hexchat.get_info("channel")

    if msg.get("join"):
        debug(
            f"  ## JOIN: {client}, known user: "
            f"{list_user.__dict__ if list_user else None}",
        )

        # Either a) the user is still unknown,
        # or b) the user is known under a different client,
        # and we have a real join message.
        if not list_user or (
            client != f":{list_user.nick}!{list_user.host}"
            and msg.get("join") != "auto"
        ):
            command(f"RECV {client} JOIN {channel}")
            if VOICE_FAKE_NICKS:
                # Use voice to mark bridged (fake) users.
                command(f"RECV :{bot} MODE {channel} +v {user}")
    else:
        debug(
            f"  ## LEFT: {client}, known user: "
            f"{list_user.__dict__ if list_user else None}",
        )

        # The user is still unknown or is known under the present client name.
        # This is a quit.
        if not list_user or client == f":{list_user.nick}!{list_user.host}":
            command(f"RECV {client} QUIT :Bridged {bot} user has left")

    return True


# Initialize.
for event in (
    "Channel Action",
    "Channel Action Hilight",
    "Channel Message",
    "Channel Msg Hilight",
):
    hexchat.hook_print_attrs(
        event,
        message_callback,
        event,
        priority=hexchat.PRI_HIGHEST,
    )
